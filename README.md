## Setup Steps

1. Install the node packages
```javascript
npm install connect serve-static
```
2. Run with Node JS
```javascript 
node server.js
```
3. Open the following hyperlink in a browser
http://localhost:8080/index.html